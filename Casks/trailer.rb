class Trailer < Cask
  url 'http://dev.housetrip.com/trailer/trailer107.zip'
  homepage 'http://dev.housetrip.com/trailer/'
  version '1.0.7'
  sha1 '880b14ce4d4e6299f767aa997a30d83cd6e60575'
  link 'Trailer.app'
end
